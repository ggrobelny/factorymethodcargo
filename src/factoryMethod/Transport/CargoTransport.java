package factoryMethod.Transport;

public interface CargoTransport {
    // interfejs, ktory posiada 1 metode typu void
    void process();
}
